var gulp = require("gulp");
var sass = require("gulp-sass")(require('sass'));
var rename = require("gulp-rename");

function css() {
    return gulp.src('assets/scss/main.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(rename(function (path) {
            path.basename = "style";
            path.extname = ".min.css";
        }))
        .pipe(gulp.dest('assets/css/'));
}

function watch() {
    css();
    gulp.watch('assets/scss/main.scss', css);
}

gulp.task('default', watch);