@extends('admin.layouts.main', ['title' => 'Tasks', 'page'=> 'Task'])
@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h3 class="card-title">Bordered Table</h3>
            @can('create', \App\Models\Task::class)
            <button class="btn btn-primary">Thêm</button>
            @endcan
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Task</th>
                        <th>Estimate</th>
                        <th style="width: 40px">Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tasks as $key => $task)
                    <tr>
                        <td>{{ $key +1 }}</td>
                        <td>{{ $task->title }}</td>
                        <td>
                            {{ $task->estimate }}
                        </td>
                        <td>{{ $task->description }}</td>
                        <td>
                            @can('update', $task)
                            <button class="btn btn-info">Sửa</button>
                            @endcan
                            @can('delete', $task)
                            <button class="btn btn-danger">Xóa</button>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- {{ $tasks->links() }} -->
        </div>

        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection