This is page products
<div class="container">
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col">Description</th>
    </tr>
  </thead>
  <tbody>
    @foreach( $products as $key => $value) 
    <tr>
      <th scope="row"> {{ $key + 1 }} </th>
      <td> {{ $value->name }} </td>
      <td> {{ $value->price }} </td>
      <td> {{ $value->description }} </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>