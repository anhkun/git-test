<?php

namespace Database\Factories;

use App\Models\ProductColor;
use App\Models\Product;
use App\Models\Color;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductColorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductColor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id'=>Product::all()->random()->id,
            'color_id' => Color::all()->random()->id
        ];
    }
}
