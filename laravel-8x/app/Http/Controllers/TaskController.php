<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index() {
        $tasks = Task::paginate(15);
        return view('admin.pages.task.index', compact('tasks'));
    }

    public function create() {
        $this->authorize('create',Task::class);
        return view('admin.pages.task.create');
    }

    public function update(Task $task) {
        $this->authorize('update', $task);
    }
}
