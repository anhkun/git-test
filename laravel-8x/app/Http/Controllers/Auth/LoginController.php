<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request) {
        $credentials_option = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ],[
            'email.required' => 'Email không được để trống !!',
            'email.email' => 'Email phải đúng định dạng !!',
            'password.required' => 'Password không được để trống !!'
        ]);
        // [ 'email' => $request->email, 'password' => $request->password] =$credentials
        if(Auth::attempt($credentials_option)) {
            $request->session()->regenerate();
            $request->session()->flash('status', 'Đăng nhập thành công !!');
            return redirect()->intended('admin');
        }
        return back()->withErrors([
            'message' => 'Thông tin đăng nhập chưa đúng.Vui lòng kiểm tra lại !!'
        ])->withInput();
    }

    public function logout() {
        Auth::logout();
        session()->flush();
        return redirect()->route('login');
    }
}
