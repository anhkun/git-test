<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\TaskController;
use App\Models\Task;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'], function() {
    Route::group([
        'as' => 'admin.',
        'prefix' => 'admin'
    ],
    function(){
        Route::get('/', function () {
            return view('admin.dashboard');
        })->name('dashboard');
        Route::get('task', [TaskController::class, 'index'])->name('task.index');
        Route::get('task/create', [TaskController::class, 'create'])->name('task.create');
        Route::get('task/update', [TaskController::class, 'update'])->name('task.update');
    });
    Route::get('/logout', [LoginController::class, 'logout']);
});


Route::get('/admin/login', function () {
    return view('admin.auth.login');
})->name('login');

Route::post('/admin/login', [LoginController::class, 'login'])->name('auth.login');
Route::get('/403', function() {
    return view('403');
})->name('403');